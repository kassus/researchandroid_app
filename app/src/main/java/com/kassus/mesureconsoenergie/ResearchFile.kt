package com.kassus.mesureconsoenergie


import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.File
import jdk.nashorn.internal.runtime.ScriptingFunctions.readLine
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL


class ResearchFile : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_research_file)
        getCreatedDate();

        val btn_upload = findViewById(R.id.uploadButton) as Button;

        // The click on the button  download csv file

        btn_upload.setOnClickListener {
            getResearchFile();

        }

    }



   fun  postData(){

       val upLoadServerUri = "http://www.localhost:3000/researchFile/";

           val url = URL(upLoadServerUri)

           val urlConnection = url.openConnection() as HttpURLConnection
           urlConnection.setRequestMethod("POST")

           urlConnection.connect()
           if (urlConnection != null) {
               urlConnection.disconnect()
           }
       }


    fun getResearchFile(){
        //if there is no SD card, create new directory objects to make directory on device
        if(Environment.getExternalStorageState() != null) {

           val files : File =  File(getFilesDir(), "/trepn/");

            Toast.makeText(this, " test file3" + files, Toast.LENGTH_SHORT).show()

    }
}





    // Get the reference of the name textwiew
    //val fileName = findViewById(R.id.name) as TextView;


    // Get the reference of the description textwiew
    //val description = findViewById(R.id.description) as TextView;


  fun getCreatedDate(){

      // Get the reference of the createddate textwiew
      val createDate = findViewById(R.id.createdDate) as TextView;

          val time  = System.currentTimeMillis();
            createDate.text = " Date de création : " +time.toString();
      }

    }


