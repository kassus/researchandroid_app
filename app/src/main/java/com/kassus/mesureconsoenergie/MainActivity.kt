package com.kassus.mesureconsoenergie

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.Snackbar
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*
import java.util.Collections.swap

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private val handler = Handler()
    // Declare an array for the sort

    var userArrayList  = arrayListOf<String>();

    var isTreatment : Boolean = false;



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }

        val toggle = ActionBarDrawerToggle(
            this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        getRamdomNumberArray();

        //start trepn profiler service

      val startTrepnProfiler = Intent()
        startTrepnProfiler.setClassName("com.quicinc.trepn", "com.quicinc.trepn.TrepnService")
        startService(startTrepnProfiler)



        // Get the reference of the saveResult
        val btn_saveResult = findViewById(R.id.btn_saveResult) as Button


        // Get the reference of the saveFile
        val btn_saveFile = findViewById(R.id.btn_saveFile) as Button

        // Get the reference of the disConnect
        val btn_disconnect = findViewById(R.id.btn_disconnect) as Button


        // Get the reference of the sortButton
        val btn_click_tri = findViewById(R.id.btn_click_tri) as Button



        // The click on the button  exit the app

       btn_disconnect.setOnClickListener {
            finish();
            System.exit(0);

        }

        // The click on the saveResult  open save result form

        btn_saveResult.setOnClickListener {
            if(isTreatment == true){

            val intent = Intent(this.getBaseContext(),  Treatment::class.java)
                intent.putExtra("startTime",startTime.text)
                intent.putExtra("endTime",endTime.text)
                intent.putExtra("spendTime",spendTime.text)
            // start your next activity
            startActivity(intent)
            }else {
                Toast.makeText(this, "The treatment must be done before saving results", Toast.LENGTH_SHORT).show()

            }

        }

        // The click on the saveFile  open save file form

        btn_saveFile.setOnClickListener {

            if(isTreatment == true){
                val intent = Intent(this, ResearchFile::class.java)
                // start your next activity
                startActivity(intent)

            }else {
                Toast.makeText(this, "The treatment must be done before saving files", Toast.LENGTH_SHORT).show()

            }



        }

        // The click on the button  start profilling

        btn_click_tri.setOnClickListener {

            // Create a new intent for starting trepn profiling service

            val startprofiling = Intent("com.quicinc.trepn.start_profiling")

            // Send startProfiling to broadcast

            sendBroadcast(startprofiling)


            val group = findViewById<View>(R.id.radioGroupTypeDeTri) as RadioGroup
            when (group.getCheckedRadioButtonId()) {
                R.id.triCocktail -> coktailSortMain()
                R.id.triParInsertion -> insertSortMain()

                else -> Log.e("message d'erreur", "Pas de sélection  de méthodes de tri")
            }

        }


    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_camera -> {
                // Handle the camera action
            }
            R.id.nav_gallery -> {

            }
            R.id.nav_slideshow -> {

            }
            R.id.nav_manage -> {

            }
            R.id.nav_share -> {

            }
            R.id.nav_send -> {

            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    // This is use for the cocktail sorting

    fun cocktailSort(a: ArrayList<String>) {

        // Get the reference of the startTime textwiew
        val startTime = findViewById(R.id.startTime) as TextView


        // Get the reference of the endTime textwiew
        var endTime = findViewById(R.id.endTime) as TextView



        // Get the reference of the endTime textwiew
        val spendTime = findViewById(R.id.spendTime) as TextView


        // Get the start Time

        val start = System.currentTimeMillis();

        startTime.text = " Temps de début : " + start.toString();

        fun swap(i: Int, j: Int) {
            val temp = a[i]
            a[i] = a[j]
            a[j] = temp
        }
        do {
            var swapped = false
            for (i in 0 until a.size - 1)
                if (a[i] > a[i + 1]) {
                    swap(i, i + 1)
                    swapped = true
                }
            if (!swapped) break
            swapped = false
            for (i in a.size - 2 downTo 0)
                if (a[i] > a[i + 1]) {
                    swap(i, i + 1)
                    swapped = true
                }
        } while (swapped)

        Toast.makeText(this, "The execution of the sort is finish", Toast.LENGTH_SHORT).show()

        // Récuperation du temps de l'opération

        val end = System.currentTimeMillis();

        endTime.text  = " Temps de fin : " + end.toString();


        // Get the spendTime

        spendTime.text =  "Temps écoulé : " + (end - start).toString();



    }

    // This is use to start cocktailSort

    fun  coktailSortMain() {

        isTreatment = true;

        Handler().postDelayed({


            cocktailSort(userArrayList)

            Log.e("result coktailsort", userArrayList.joinToString(", "))

      }, 10000)
    }


    // This is use to start insertSort


   fun  insertSort(a: ArrayList<String>) {

       // Get the reference of the startTime textwiew
       val startTime = findViewById(R.id.startTime) as TextView


       // Get the reference of the endTime textwiew
       val endTime = findViewById(R.id.endTime) as TextView



       // Get the reference of the endTime textwiew
       val spendTime = findViewById(R.id.spendTime) as TextView


        // Get the start Time

       val  start  = System.currentTimeMillis();

       startTime.text =  " Temps de début : " + start.toString();

        var i: Int
        var arrayItem : String
        for (j in 1 until userArrayList.size) {
            arrayItem = userArrayList[j]
            i = j - 1
            while (i >= 0 && userArrayList[i] > arrayItem) {
                userArrayList[i + 1] = userArrayList[i]
                i--
            }

            userArrayList[i + 1] = arrayItem

        }

        Toast.makeText(this, "The execution of the sort is finish", Toast.LENGTH_SHORT).show()
        // Récuperation du temps de l'opération

       val end = System.currentTimeMillis();

       endTime.text  = " Temps de fin : " + end.toString();

       // Get the spendTime

       spendTime.text =  "Temps écoulé : " + (end - start).toString();


    }


    // This is use to start insertSort

    fun  insertSortMain() {

        isTreatment = true;

        Handler().postDelayed({

            insertSort(userArrayList)
            Log.e("result insertSort", userArrayList.joinToString(", "))

        }, 10000)
    }



    fun getRamdomNumberArray() {

        // Init random array number

        for (k in 1  until 50000){
            val randomNummer : Int = ( 0 until 50000).random()

            userArrayList.add(randomNummer.toString())

        }
    }


}


