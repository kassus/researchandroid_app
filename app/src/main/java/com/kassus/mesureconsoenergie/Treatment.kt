package com.kassus.mesureconsoenergie

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.content.Intent
import android.widget.Toast
import java.net.HttpURLConnection
import java.net.URL

class Treatment : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_treatment)
        getResultFromIntent();

    }


 fun getResultFromIntent(){

     // Get the reference of the startTime textwiew
     val beguin = findViewById(R.id.start) as TextView;


     // Get the reference of the endTime textwiew
     val ended = findViewById(R.id.end) as TextView;


     // Get the reference of the endTime textwiew
     val spended = findViewById(R.id.spend) as TextView;


     // Get the reference of the description textwiew
     val description = findViewById(R.id.description) as TextView;


     var extras = getIntent().extras;
     if (extras != null){
         //Get Treatment result

         val start = extras.getString("startTime");
         val end = extras.getString("endTime");
         val spend = extras.getString("spendTime");

         Toast.makeText(this, start, Toast.LENGTH_SHORT).show()

             // Init the treatment form
             beguin.text   = start;
             ended.text   = end;
             spended.text   = spend;


     }

 }

    fun  postData(){
        val upLoadServerUri = "http://www.localhost:3000/treatment/";

        val url = URL(upLoadServerUri)

        val urlConnection = url.openConnection() as HttpURLConnection
        urlConnection.setRequestMethod("POST")

        urlConnection.connect()
        if (urlConnection != null) {
            urlConnection.disconnect()
        }
    }



}
